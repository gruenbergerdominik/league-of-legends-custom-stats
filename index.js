const electron = require('electron')
const path = require('path')
const { app, BrowserWindow } = require('electron')

function createWindow () {
  // Erstelle das Browser-Fenster.
  let win = new BrowserWindow({
    width: 1100,
    height: 700,
    resizable: false,
    autoHideMenuBar: true,
    backgroundColor: "#000",
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false,
      allowRunningInsecureContent: true
    },
    titleBarStyle: "hidden"
  })
  win.setIcon(path.join(__dirname, 'ico.png'));
  // and load the index.html of the app.
  win.loadFile('index.html')
}

app.commandLine.appendSwitch('ignore-certificate-errors');
app.on('ready', createWindow)